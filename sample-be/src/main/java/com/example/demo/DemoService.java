package com.example.demo;

import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class DemoService {

	private DemoRepository demoRepository;

	public DemoService(DemoRepository demoRepository) {
		this.demoRepository = demoRepository;
	}

	public List<Mahasiswa> findAll() {
		return demoRepository.findAll();
	}

}
