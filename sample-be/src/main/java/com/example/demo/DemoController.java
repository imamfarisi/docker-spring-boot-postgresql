package com.example.demo;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("mahasiswa")
public class DemoController {

	private DemoService demoService;

	public DemoController(DemoService demoService) {
		this.demoService = demoService;
	}

	@GetMapping
	public ResponseEntity<?> findAll() {
		List<Mahasiswa> dataMahasiswa = demoService.findAll();
		return new ResponseEntity<List<Mahasiswa>>(dataMahasiswa, HttpStatus.OK);
	}

}
