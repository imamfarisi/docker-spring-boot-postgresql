## How to start
1. Generate .jar file : <br>
1.1. cd sample-be <br>
1.2. mvn clean package -DskipTests <br>

2. Generate app image : <br>
2.1. docker build --tag {username_docker_hub}\sample-be:0.1 . <br>

3. Generate db image : <br>
3.1. cd ../sample-db <br>
3.2. docker build --tag {username_docker_hub}\sample-db:0.1 . <br>

4. Generate container and start the application : <br>
4.1. cd ..<br>
4.2. docker compose up <br>