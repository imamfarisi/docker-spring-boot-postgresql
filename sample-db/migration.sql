DROP TABLE IF EXISTS public.mahasiswa;

CREATE TABLE public.mahasiswa (
	id serial NOT NULL,
	nim varchar NULL,
	nama varchar NULL,
	waktu_masuk date NULL,
	CONSTRAINT mahasiswa_pkey PRIMARY KEY (id)
);

INSERT INTO public.mahasiswa (nim,nama,waktu_masuk) VALUES
	 ('11111','Mahasiswa 1','2022-03-26'),
	 ('11112','Mahasiswa 2','2022-03-26'),
	 ('11113','Mahasiswa 3','2022-03-26'),
	 ('11114','Mahasiswa 4','2022-03-26'),
	 ('11115','Mahasiswa 5','2022-03-26'),
	 ('11116','Mahasiswa 6','2022-03-26'),
	 ('11117','Mahasiswa 7','2022-03-26'),
	 ('11118','Mahasiswa 8','2022-03-26'),
	 ('11119','Mahasiswa 9','2022-03-26');